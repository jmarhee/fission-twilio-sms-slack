#Fisson POST task
import json
import requests
import urllib
from flask import request, redirect
from twilio.rest import Client

CHANNEL_NAME="twilio"
SLACK_TOKEN=""
TWILIO_SENDER="" #Number with Country code
LIMIT_TO_CC="" #i.e. +1

def slack_callout(response_url,response):
    try:
        response = { "text" : "%s" %(response), "channel": "#%s" % (CHANNEL_NAME), "username": "Twilio", "icon_url": ""}
        slack_response = requests.post(response_url,json=response, headers={'Content-Type': 'application/json'})
        return slack_response.status_code
    except:
        return "Slack Callout Failed."

def twilio_conn(send_message,recipient)

        rcpt = LIMIT_TO_SENDER_CC + recipient

        message = client.messages \
        .create(
                body='%s' % (send_message),
                from_=TWILIO_SENDER,
                to='%s' % (rcpt)
        )

        return message.sid

def main():

    params = urllib.parse.parse_qs(str(request.get_data()))

    incoming_token = params["b'token"][0]
    channel_name = params['channel_name'][0]
    response_url = params['response_url'][0]
    message = params['text'][0]

    options = ["Send message format:\n`send:1234567890 message`\n"]

    if message.startswith("send:") == True:
        rcpt_number = message.split(":")[1]
        number = rcpt_number.split(":")[1][0:10]
        message = rcpt_number.replace(number,'').split(":")[1]
        response = twilio_conn(message,number)
    else:
        response = "Available Commands:\n" + '\n'.join('({}) {}'.format(*k) for k in enumerate(options, 1))

    if incoming_token == SLACK_TOKEN and channel_name == CHANNEL_NAME:
        slack_callout(response_url,response)
    else:
        return "Invalid Parameter"
        exit(1)