#Fission GET Task
#Configure as Webhook in Twilio UI pointed to Fission Router URI 
# i.e. <https://support.twilio.com/hc/en-us/articles/223136047-Configuring-Phone-Numbers-to-Receive-and-Respond-to-SMS-and-MMS-Messages#webhook>
import json
import requests
from flask import request, redirect

INCOMING_WEBHOOK_URL=""
CHANNEL_NAME="twilio"
SLACK_TOKEN=""

def slack_callout(response):
    try:
        response = { "text" : "%s" %(response), "channel": "#%s" % (CHANNEL_NAME), "username": "Twilio", "icon_url": ""}
        slack_response = requests.post(INCOMING_WEBHOOK_URL,json=response, headers={'Content-Type': 'application/json'})
        return slack_response.status_code
    except:
        return "Slack Callout Failed."

def main():

    sender = str(request.args.get("From"))
    recipient = str(request.args.get("To"))
    body = str(request.args.get("Body"))

    message = "From %s to %s: `%s`" % (sender,recipient,body)

    slack_callout(message)